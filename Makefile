all:
	$(MAKE) -C debian-tags $@
	$(MAKE) -C kali-tags $@

install:
	$(MAKE) -C debian-tags $@
	$(MAKE) -C kali-tags $@
	mkdir -p $(DESTDIR)/etc/debtags/sources.list.d
	cp sources.list $(DESTDIR)/etc/debtags/sources.list.d/kali

clean distclean:
	$(MAKE) -C debian-tags $@
	$(MAKE) -C kali-tags $@

update:
	$(MAKE) -C debian-tags $@
